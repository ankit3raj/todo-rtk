import { Checkbox } from "antd";

type Props = {
  onChange: Function;
  children: boolean;
};

const Status = ({ onChange: onBtnClick, children }: Props) => {
  return (
    <>
      <Checkbox onChange={(e) => onBtnClick(e)}>{children}</Checkbox>
    </>
  );
};
export default Status;
