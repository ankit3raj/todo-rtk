import InputField from "../Input/input";
import Buttons from "../Button/button";
import { useDispatch } from "react-redux";
import { addToDo } from "../../Redux/todoSlice";
import { successAlert } from "../../Redux/todoSlice";
import { useState } from "react";
import "./addTodo.css";
import Modals from "../Modal/modal";
import { useSelector } from "react-redux";
import { Typography } from "antd";
import { Button } from "antd";
const { Text } = Typography;

const AddTodo = () => {
  const dispatch = useDispatch();
  const [newTodoLabel, setNewTodoLabel] = useState("");
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [open, setOpen] = useState(false);
  // const alertType = useSelector((state: any) => state.toDo.alertType);
  const alert = useSelector((state: any) => state.toDo.alert);

  const showModal = () => {
    setOpen(true);
  };

  const handleOk = () => {
    if (newTodoLabel === "") {
      dispatch(successAlert("Input field can't be empty"));
      setOpen(true);
    } else {
      dispatch(addToDo(newTodoLabel));
      dispatch(successAlert("Todo added successfully"));
      setNewTodoLabel("");
      // dispatch(successAlert(""));
      setTimeout(() => {
        setConfirmLoading(true);
        setOpen(false);
        setConfirmLoading(false);
      }, 1000);
    }

    console.log("in Ok fn");
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setOpen(false);
    dispatch(successAlert(""));
  };

  return (
    <>
      <Button
        // style={{ background: "#CDFCF6" }}
        type="primary"
        onClick={() => {
          showModal();
        }}
      >
        Add Todo
      </Button>
      <Modals
        title="Add Todo"
        onOk={handleOk}
        open={open}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <p>Add Your Task Here</p>

        {
          <InputField
            value={newTodoLabel}
            onChange={(e: any) => setNewTodoLabel(e.target.value)}
          />
        }
        <div>
          <Text type="danger">{alert}</Text>
        </div>
      </Modals>
    </>
  );
};
export default AddTodo;
