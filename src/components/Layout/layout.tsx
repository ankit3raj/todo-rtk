import React from "react";
import { UserOutlined } from "@ant-design/icons";
import { Layout, Menu, Typography } from "antd";
import Avatar from "antd/lib/avatar/avatar";
import { Footer } from "antd/lib/layout/layout";
import "./layout.css";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { successAlert } from "../../Redux/todoSlice";
import { useDispatch } from "react-redux";
import BreadCrumb from "../../components/Breadcrumb/breadcrumb";
import TodoList from "../TodoList/todoList";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import PlotlyGraph from "../../components/Plotly/plotly";
import ContactForm from "../../components/Form/form";

const { Header, Content, Sider } = Layout;
const { Title } = Typography;

const LayoutApp: React.FC = () => {
  const dispatch = useDispatch();

  const alertType = useSelector((state: any) => state.toDo.alertType);
  const alert = useSelector((state: any) => state.toDo.alert);
  return (
    <BrowserRouter>
      <Layout>
        <Header style={{ padding: 10 }}>
          <Avatar style={{ float: "right" }} icon={<UserOutlined />} />
          <Title style={{ color: "white" }} level={3}>
            TODO APP
          </Title>
        </Header>
        <Layout>
          <Sider
            breakpoint="sm"
            collapsedWidth="0"
            style={{ background: "white" }}
          >
            <Menu defaultSelectedKeys={["DashBoard"]} mode="inline">
              <Menu.Item key="DashBoard">
                <Link to="/dashboard">DashBoard</Link>
              </Menu.Item>
              <Menu.Item key="Graph">
                <Link to="/Graph">Graph</Link>
              </Menu.Item>
              <Menu.Item key="Form">
                <Link to="/Form">Form</Link>
              </Menu.Item>
            </Menu>
          </Sider>

          <Layout>
            <Content style={{ padding: "0 50px" }}>
              {/* <Breadcrumb style={{ margin: "16px 0" }}>
                <Breadcrumb.Item>DashBoard</Breadcrumb.Item>
                <Breadcrumb.Item>Graph</Breadcrumb.Item>
                <Breadcrumb.Item>Form</Breadcrumb.Item>
              </Breadcrumb> */}
              <BreadCrumb />
              <div
                className="content"
                // style={{ background: "#ffff", padding: 24, minHeight: 635 }}
              >
                {/* <TodoList /> */}
                <div
                  style={{
                    color: alertType === "success" ? "green" : "red",
                    marginRight: "2rem",
                  }}
                >
                  {alert}
                  {alert && (
                    // eslint-disable-next-line
                    <a onClick={() => dispatch(successAlert(""))}>clear</a>
                  )}
                </div>
                <Routes>
                  <Route path="/" element={<Navigate to="/dashboard" />} />

                  <Route path="/dashboard" element={<TodoList />} />
                  <Route path="/Graph" element={<PlotlyGraph />} />
                  <Route path="/Form" element={<ContactForm />} />
                </Routes>
              </div>
            </Content>
            <Footer style={{ textAlign: "center" }}>
              Todo App created with AntDesign
            </Footer>
          </Layout>
        </Layout>
      </Layout>
    </BrowserRouter>
  );
};

export default LayoutApp;
