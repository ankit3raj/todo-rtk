import { Button } from "antd";
import "./button.css";
type Props = {
  style: React.CSSProperties;
  onClick: Function;
  children: string | React.ReactElement;
};

const Buttons = ({ onClick: onBtnClick, children, style }: Props) => {
  return (
    <>
      <Button style={style} onClick={() => onBtnClick()}>
        {children}
      </Button>
    </>
  );
};
export default Buttons;
