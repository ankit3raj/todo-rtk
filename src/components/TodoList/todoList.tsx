import React from "react";
import { useSelector } from "react-redux";
import AddTodo from "../AddTodo/addTodo";
import { sortTodo } from "../../Redux/todoSlice";
import "./todoList.css";
import TodoItem from "../TodoItem/todoItem";
import { Typography } from "antd";

import { Select } from "antd";
import { useDispatch } from "react-redux";

const { Title } = Typography;
const { Option } = Select;

const TodoList: React.FC = () => {
  const dispatch = useDispatch();

  const todos = useSelector((state: any) => state.toDo.todoList);
  // const alertType = useSelector((state: any) => state.toDo.alertType);
  // const alert = useSelector((state: any) => state.toDo.alert);
  const countCompleted = todos.filter(
    (item: any) => item.completed === "completed"
  ).length;

  return (
    <>
      <Title level={2} style={{ textAlign: "center" }}>
        To-Do List
      </Title>

      <div
        style={{ display: "flex", justifyContent: "flex-end", width: "13rem" }}
      >
        <AddTodo />

        <Select
          placeholder="Sort By"
          defaultValue="Sort By"
          style={{ width: "7rem", margin: "0 1rem" }}
          onChange={() => {
            dispatch(sortTodo(""));
            console.log("onchange called");
          }}
        >
          <Option value="todo">todo</Option>
        </Select>
      </div>
      <Title level={5} style={{ marginTop: "1rem" }}>
        Completed Task: {countCompleted}
      </Title>
      <Title level={5}>Total Task: {todos.length}</Title>
      <div className="card">
        {todos.map((todo: any) => (
          <div className="todo" key={todo.id}>
            <TodoItem todo={todo} key={todo.id} />
          </div>
        ))}
      </div>
    </>
  );
};
export default TodoList;
