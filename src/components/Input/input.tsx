import { Input } from "antd";

type Props = {
  onChange: Function;
  value: string;
};

const InputField = ({ onChange: labelChange, value }: Props) => {
  return (
    <>
      <Input
        style={{ width: "50%" }}
        type="text"
        value={value}
        onChange={(e) => labelChange(e)}
      />
    </>
  );
};
export default InputField;
