import { Typography } from "antd";
import Modals from "../Modal/modal";
import { useState } from "react";
import Buttons from "../Button/button";
import { Form, Input, Button, Radio, TreeSelect } from "antd";
import { addForm, deleteForm } from "../../Redux/formSlice";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { Card } from "antd";
import "./form.css";
import { message } from "antd";

const success = () => {
  message.success("New User Is Added", 10);
};
const ContactForm = () => {
  const [form] = Form.useForm();
  const { Title } = Typography;
  const [newTodoLabel, setNewTodoLabel] = useState("");
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const formData = useSelector((state: any) => state.form.formList);

  const showModal = () => {
    setOpen(true);
  };

  const handleSubmit = (event: any) => {
    dispatch(addForm(event));
    setOpen(false);
  };
  const handleDelete = (id: any) => {
    dispatch(deleteForm(id));
  };
  const handleOk = () => {
    if (newTodoLabel === "") {
      setOpen(true);
    } else {
      setNewTodoLabel("");
      setTimeout(() => {
        setConfirmLoading(true);
        setOpen(false);
        setConfirmLoading(false);
      }, 1000);
    }

    console.log("in Ok fn");
  };

  const handleCancel = () => {
    setOpen(false);
  };
  return (
    <>
      <Title level={2} style={{ textAlign: "center" }}>
        Registration Form
      </Title>
      <Buttons
        style={{ background: "#CDFCF6" }}
        onClick={() => {
          showModal();
        }}
      >
        Register User
      </Buttons>
      <Modals
        title="User Registration"
        onOk={handleOk}
        open={open}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <Form
          onFinish={handleSubmit} // Ant Design's Form Component
          name="contact-us"
          layout="vertical"
          form={form}
          wrapperCol={{
            span: 6,
          }}
          style={{
            marginTop: 20,
            paddingBottom: 10,
            paddingLeft: 30,
            paddingRight: 30,
          }}
        >
          <Form.Item // Form Item (First Name)
            label="Name"
            name="Name"
            required
            tooltip="This is a required field"
            rules={[
              {
                required: true,
                message: "Please enter your name!",
              },
            ]}
            style={{ width: 1000 }}
          >
            <Input placeholder="First Name" />
          </Form.Item>

          <Form.Item // Form Item (Email)
            label="Email"
            name="email"
            required
            tooltip="This is a required field"
            rules={[
              {
                required: true,
                message: "Please enter your email!",
                type: "email",
              },
            ]}
            style={{ width: 1000 }}
          >
            <Input placeholder="Email" />
          </Form.Item>
          <Form.Item label="Gender" name="gender">
            <Radio.Group>
              <Radio value="Male"> Male </Radio>
              <Radio value="Female"> Female </Radio>
              <Radio value="Others"> Other </Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="City" name="city">
            <TreeSelect
              treeData={[
                {
                  title: "Karnataka",
                  value: "karnataka",
                  children: [{ title: "Bangalore", value: "bangalore" }],
                },
                {
                  title: "Telengana",
                  value: "telengana",
                  children: [{ title: "Hyderabad", value: "hyderabad" }],
                },
              ]}
              style={{ width: 150 }}
            />
          </Form.Item>
          {/* 
          <Form.Item label="Hobbies" name="hobbies">
            <div style={{ display: "flex" }}>
              <Checkbox value="coding">Coding</Checkbox>
              <Checkbox value="gaming">Gaming</Checkbox>
            </div>
          </Form.Item> */}
          <Form.Item>
            <Button type="primary" htmlType="submit" onClick={success}>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modals>

      <div className="form">
        {formData.map((data: any) => (
          <div className="formList" key={data.id}>
            <Card
              className="form-list"
              title="User Registration"
              style={{
                width: 300,

                borderRadius: "1rem",
                // border: "1px solid grey",
                backgroundColor: "#B4CDE6",
                // boxShadow: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
                boxShadow:
                  "rgba(0, 0, 0, 0.17) 0px -23px 25px 0px inset, rgba(0, 0, 0, 0.15) 0px -36px 30px 0px inset, rgba(0, 0, 0, 0.1) 0px -79px 40px 0px inset, rgba(0, 0, 0, 0.06) 0px 2px 1px, rgba(0, 0, 0, 0.09) 0px 4px 2px, rgba(0, 0, 0, 0.09) 0px 8px 4px, rgba(0, 0, 0, 0.09) 0px 16px 8px, rgba(0, 0, 0, 0.09) 0px 32px 16px",
              }}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems: "center",
                }}
              >
                <Title level={4}>
                  Name: <span>{data.Name}</span>
                </Title>
              </div>
              <Title level={4}>
                Email: <span>{data.email}</span>
              </Title>
              <Title level={4}>
                Gender: <span>{data.gender}</span>
              </Title>
              <Title level={4}>
                City: <span>{data.city}</span>
              </Title>

              <Title level={5}>Form Action</Title>
              <Button
                style={{ backgroundColor: "#CF0A0A" }}
                onClick={() => handleDelete(data.id)}
              >
                Delete
              </Button>
            </Card>
          </div>
        ))}
      </div>
    </>
  );
};

export default ContactForm;
