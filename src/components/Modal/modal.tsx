import { Modal } from "antd";

type Props = {
  open: boolean;
  confirmLoading: boolean;

  onCancel: Function;
  onOk: Function;
  title?: string;
  children: React.ReactNode;
};
const Modals = ({
  open: isOpen,
  confirmLoading: isLoading,
  onCancel: handleCancel,
  onOk: onBtnClick,
  title,
  children,
}: Props) => {
  return (
    <>
      <Modal
        title={title}
        open={isOpen}
        onOk={() => onBtnClick()}
        confirmLoading={isLoading}
        onCancel={() => handleCancel()}
      >
        {children}
      </Modal>
    </>
  );
};
export default Modals;
