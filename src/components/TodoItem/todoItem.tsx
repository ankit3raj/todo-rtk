import { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { deleteToDo, editTodo } from "../../Redux/todoSlice";
import Buttons from "../Button/button";
import type { CheckboxChangeEvent } from "antd/es/checkbox";
import { setTodoStatus } from "../../Redux/todoSlice";
import Status from "../Status/status";
import { Typography } from "antd";
import "./todoItem.css";
import Modals from "../Modal/modal";
import { Card } from "antd";

import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

type Props = {
  todo: any;
};
const { Title } = Typography;

const TodoItem = ({ todo }: Props) => {
  const dispatch = useDispatch();
  const [editing, setEditing] = useState(false);
  const editingInput = useRef<HTMLInputElement>(null);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [open, setOpen] = useState(false);

  const showModal = () => {
    setOpen(true);
  };

  const handleOk = (id: any) => {
    setConfirmLoading(true);
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
    }, 1000);
    dispatch(deleteToDo(id));
  };
  const handleCancel = () => {
    console.log("Clicked cancel button");
    setOpen(false);
  };

  const handleUpdate = (id: any, val: string) => {
    dispatch(editTodo({ id: id, value: val }));
    setEditing(false);
  };

  return (
    <>
      <div className="todo-list">
        <Card
          className="todo-list"
          title="Todo"
          style={{
            width: 300,

            borderRadius: "1rem",
            // border: "1px solid grey",
            backgroundColor: "#B4CDE6",
            // boxShadow: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
            boxShadow:
              "rgba(0, 0, 0, 0.17) 0px -23px 25px 0px inset, rgba(0, 0, 0, 0.15) 0px -36px 30px 0px inset, rgba(0, 0, 0, 0.1) 0px -79px 40px 0px inset, rgba(0, 0, 0, 0.06) 0px 2px 1px, rgba(0, 0, 0, 0.09) 0px 4px 2px, rgba(0, 0, 0, 0.09) 0px 8px 4px, rgba(0, 0, 0, 0.09) 0px 16px 8px, rgba(0, 0, 0, 0.09) 0px 32px 16px",
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "start",
              alignItems: "center",
            }}
          >
            <Title level={4}>
              Task Name: {!editing && <span>{todo.content}</span>}
              {editing && (
                <input
                  type="text"
                  defaultValue={todo.content}
                  ref={editingInput}
                />
              )}
            </Title>
          </div>
          <Title level={4}>
            Task Status:{" "}
            <Status
              onChange={(e: CheckboxChangeEvent) =>
                dispatch(
                  setTodoStatus({
                    id: todo.id,
                    completed: e.target.checked ? "completed" : "notCompleted",
                  })
                )
              }
            >
              {todo.completed}
            </Status>
          </Title>

          <Title level={5}>Task Action</Title>
          <Buttons
            style={{ background: "#E0144C", marginRight: "1rem" }}
            // type="primary"
            onClick={() => {
              showModal();
            }}
          >
            <DeleteOutlined />
          </Buttons>
          <Modals
            title="Delete Todo"
            onOk={() => handleOk(todo.id)}
            open={open}
            confirmLoading={confirmLoading}
            onCancel={handleCancel}
          >
            <p>Are you sure you want to delete this task:{todo.content}</p>
          </Modals>
          {!editing && (
            <Buttons
              style={{ background: "white" }}
              onClick={() => setEditing(true)}
            >
              <EditOutlined />
            </Buttons>
          )}
          {editing && (
            <Buttons
              style={{ background: "white" }}
              onClick={() =>
                handleUpdate(
                  todo.id,
                  editingInput && editingInput.current
                    ? editingInput.current.value
                    : ""
                )
              }
            >
              Update
            </Buttons>
          )}
        </Card>
      </div>
    </>
  );
};

export default TodoItem;
