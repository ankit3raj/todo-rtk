import React from "react";
import Plot from "react-plotly.js";
import { useSelector } from "react-redux";

const PlotlyGraph = () => {
  const todos = useSelector((state: any) => state.toDo.todoList);
  const countCompleted = todos.filter(
    (item: any) => item.completed === "completed"
  ).length;
  const notCompleted = todos.length - countCompleted;
  return (
    <>
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        <Plot
          data={[
            {
              x: [1, 2, 3],
              y: [2, 6, 3],
              type: "scatter",
              mode: "lines+markers",
              marker: { color: "red" },
            },
            { type: "bar", x: [1, 2, 3], y: [2, 5, 3] },
          ]}
          layout={{ width: 500, height: 600, title: "A Fancy Plot" }}
        />
        <Plot
          data={[
            {
              values: [countCompleted, notCompleted],
              labels: ["completed", "Not Completed"],
              type: "pie",
            },
          ]}
          layout={{ width: 500, height: 500, title: "Todo Pie Chart" }}
        />
      </div>
      {/* <div>{todos.length}</div> */}
    </>
  );
};
export default PlotlyGraph;
