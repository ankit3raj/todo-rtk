import { configureStore } from "@reduxjs/toolkit";
import toDoReducer from "./todoSlice";
import formReducer from "./formSlice";
import storage from "redux-persist/lib/storage";
import { persistReducer, persistStore } from "redux-persist";
// import thunk from "redux-thunk";

const persistConfig = {
  key: "root",
  storage,
};
const persistedtodoReducer = persistReducer(persistConfig, toDoReducer);
const persistedFormReducer = persistReducer(persistConfig, formReducer);
export const store = configureStore({
  reducer: {
    toDo: persistedtodoReducer,
    form: persistedFormReducer,
    // middleware: [thunk],
  },
});

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
