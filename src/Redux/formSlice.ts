import { createSlice } from "@reduxjs/toolkit";

export const formSlider = createSlice({
  name: "form",
  initialState: {
    formList: [
      {
        id: 1,
        name: "ankit",
        email: "a@abc.com",
        gender: "male",
        City: "Bangalore",
      },
    ],
  },
  reducers: {
    addForm: (state, action) => {
      let newForm = action.payload;
      newForm.id = Date.now();

      state.formList.push(newForm);
    },
    deleteForm: (state, action) => {
      state.formList = state.formList.filter(
        (item) => item.id !== action.payload
      );
    },
  },
});
// Action creators are generated for each case reducer function
export const { addForm, deleteForm } = formSlider.actions;
export default formSlider.reducer;
