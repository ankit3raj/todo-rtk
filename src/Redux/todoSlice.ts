import { createSlice } from "@reduxjs/toolkit";
export const toDoSlider = createSlice({
  name: "toDo",
  initialState: {
    todoList: [{ id: 1, content: "Hi!Ankit", completed: "notCompleted " }],
    alert: "",
    alertType: "",
    successMessage: "New Task is Added",
  },
  reducers: {
    addToDo: (state, action) => {
      let newTodoList = {
        id: Date.now(),
        content: action.payload,
        completed: "notCompleted",
      };
      state.todoList.push(newTodoList);
    },
    deleteToDo: (state, action) => {
      state.todoList = state.todoList.filter(
        (item) => item.id !== action.payload
      );
    },
    editTodo: (state, action) => {
      state.todoList.forEach((item) => {
        if (item.id === action.payload.id) {
          item.content = action.payload.value;
        }
      });
    },
    setTodoStatus: (state, action) => {
      let todo = state.todoList.find((item) => item.id === action.payload.id);
      if (todo) {
        todo.completed = action.payload.completed;
      }
    },
    sortTodo: (state, action) => {
      state.todoList.sort((a: any, b: any) =>
        a.content.toLowerCase() > b.content.toLowerCase() ? 1 : -1
      );
      console.log("sortTodo");
    },
    successAlert: (state, action) => {
      state.alert = action.payload;
      state.alertType = "success";
    },
  },
});
// Action creators are generated for each case reducer function
export const {
  addToDo,
  deleteToDo,
  editTodo,
  setTodoStatus,
  sortTodo,
  successAlert,
} = toDoSlider.actions;
export default toDoSlider.reducer;
