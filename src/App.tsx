import React from "react";
import "./App.css";
import LayoutApp from "./components/Layout/layout";

const App: React.FC = () => {
  return <LayoutApp />;
};

export default App;
